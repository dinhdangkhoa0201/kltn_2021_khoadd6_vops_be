FROM openjdk:8-jdk-alpine

VOLUME /tmp

ADD target/backend-0.0.1-SNAPSHOT.jar backend.jar

ENTRYPOINT ["java", "-jar", "/backend.jar"]

